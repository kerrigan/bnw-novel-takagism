all:
	cat js/polyfill.min.js js/jquery.min.js js/particles.min.js js/typed.min.js js/artemis.min.js js/strings.js js/options.js js/storage.js js/script.js js/script_izorashka_flat.js js/script_izorashka_eaves.js js/script_izorashka_flat2.js js/script_izorashka_arbuz.js js/script_izorashka_flat3.js js/script_izorashka_mountain.js js/script_izorashka_podnos.js js/script_izorashka_kitchen.js js/script_izorashka_redbull_sanctuary.js js/script_izorashka_end.js js/monogatari.js js/main.js > dist/dist.js
	#jsmin dist/dist.js > dist/dist2.js
	#rm dist/dist.js
	#mv dist/dist2.js dist/dist.js
