script["Izorashka_Flat"] = [
  "scene izorashka_flat",
  {
    "Conditional": {
      "Condition": function() {
        return storage.izorashka_flat.first == true;
      },
      "True": "jump Izorashka_Flat_intro",
      "False": "jump Izorashka_Flat_main"
    }
  }
];

script["Izorashka_Flat_intro"] = [
    function() {
      storage.izorashka_flat.first = false;
      return true;
    },
    "centered Вы осознаете что находитесь в той же комнате.",
    "centered На этот раз включен телевизор. Вроде бы там включен первый канал.",
    "Изображение меняется на странный флаг. Звучит гимн.",
    "jump Izorashka_Flat_main"
];

script["Izorashka_Flat_main"] = [
  {
    "Choice": {
      "Dialog": "На столе стоит арбуз с квадратной дыркой, откуда вынут ломтик.Твои действия?",
      "1": {
        "Text": "Трахнуть арбуз",
        "Do": "jump arbuz_fuck",
      },
      "2": {
        "Text": "Осмотреться",
        "Do": "jump Izorashka_Flat_description"
      },
      "3": {
        "Text": "Подойти к окну",
        "Do": "jump Izorashka_eaves"
      }
    }
  }
];


script["Izorashka_Flat_description"] = [
  "У тебя за спиной стоит старая мебельная стенка, слева окно",
  {
    "Choice": {
      "0": {
        "Text": "Осмотреть стенку",
        "Do": "jump Izorashka_furniture_wall"
      },
      "1": {
        "Text": "Подойти к окну",
        "Do": "jump Izorashka_eaves"
      },
    }
  }
];

//TODO: scene
script["Izorashka_furniture_wall"] = [
  "scene flat_wall fadeIn",
	"Противоположную сторону стены занимает мебельная стенка",
	"На обычную советскую не похожа, но и стариной не отличается",
	"Полки набиты всяким оставшимся от потолочинга хламом",
  {
    "Choice": {
      "Dialog": "Чего делаем?",
      "0": {
        "Text": "Подойти к окну",
        "Do": "jump Izorashka_eaves"
      },
      "1": {
        "Text": "Нассать в рюкзак",
        "Do": "jump backpack",
        "Condition": function() {
          return storage.izorashka_flat.backpack_spoiled === false;
        }
      },
      "2": {
        "Text": "Взять мультитул",
        "Do": "jump multitool",
        "Condition": function() {
          return storage.izorashka_flat.multitool_taken === false;
        }
      },
      "3": {
        "Text": "Взять рулон полиэтиленовой пленки",
        "Do": "jump plenka",
        "Condition": function() {
          return storage.izorashka_flat.plenka_taken === false;
        }
      }
    }
  }
];


script["arbuz_fuck"] = [
  {
    "Conditional": {
      "Condition": function() {
        return storage.izorashka_flat.arbuz_spoiled;
      },
      "True": "jump arbuz_spoiled",
      "False": "jump arbuz_fuck_success"
    }
  }
];

script["arbuz_fuck_success"] = [
  "p О Х У И Т Е Л Ь Н О",
  function() {
    storage.izorashka_flat.arbuz_spoiled = true;
    return true;
  },
  "jump Izorashka_Flat"
]

script["arbuz_spoiled"] = [
  "centered Ты уже воспользовался арбузом и он выглядит непривлекательно",
  "jump Izorashka_Flat_main"
];

//TODO: показать модельку
script["backpack"] = [
  "display image backpack",
  function() {
    storage.izorashka_flat.backpack_spoiled = true;
    return true;
  },
  "Рюкзак был бесповоротно испорчен.",
  "Можешь теперь сказать, что кот тебе рюкзак обоссал",
  "Ой, у тебя же аллергия на котиков, выкручивайся теперь сам",
  "hide",
  "jump Izorashka_furniture_wall"
];

//TODO: показать модельку
script["multitool"] = [
  "display image multitool",
  "p О, это же Leatherman",
  "p Вейт, тут что-то написано:",
  "centered Произведено в подвалах Дяди Ляо",
  "p Очередная китайская подделка...",
  "Ты взял мультитул",
  "hide",
  function() {
    storage.izorashka_flat.multitool_taken = true;
    return true;
  },
  "jump Izorashka_furniture_wall"
];

//TODO: показать рулон
script["plenka"] = [
  "display image plenka",
  "Обычная пленка для заворачивания еды",
  "p Или чего-нибудь поинтереснее",
  "hide",
  function() {
    storage.izorashka_flat.plenka_taken = true;
    return true;
  },
  "jump Izorashka_furniture_wall"
];
