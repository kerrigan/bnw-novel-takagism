/**
 * ==============================
 * Your Javascript Code Goes Here
 * ==============================
 **/

 $_ready(function () {
   var videoPlayer = document.querySelector("[data-ui='player']");

   videoPlayer.addEventListener('click', function() {
     videoPlayer.pause();
     videoPlayer.currentTime = 0;
     videoPlayer.setAttribute("src", "");
     $_("[data-component='video']").removeClass("active");
   });
});
