//TODO: scene
script["Izorashka_Kitchen2"] = [
  "scene kitchen fadeIn",
  "Ты снова на кухне, тебе тут больше нечего делать",
  {
    "Choice": {
      "0": {
        "Text": "Вернуться в комнату",
        "Do": "jump Izorashka_flat4"
      }
    }
  }
];

//TODO: scene
script["Izorashka_flat4"] = [
  "scene pre_podnos",
  "Сквозняк из открытой форточки сдул с верхотуры стенки страницу с нотами",
  "display image note_sheet",
  "p <b>Нотная тетрадь Магдалены Бах</b>",
  "p Легкая музыка, чтобы играть одной рукой",
  "hide note_sheet",
  {
    "Choice": {
      "Dialog": "Напомню, что в комнате есть рояль, а ноты довольно простые. Весы снова стоят на столе.",
      "0": {
        "Text": "Сыграть менуэт",
        "Do": "jump floor_fail_dead_end"
      },
      "1": {
        "Text": "Взвесить ноты",
        "Do": "jump secret_podnos_wall"
      }
    }
  }
];


//TODO: scene
script["floor_fail_dead_end"] = [
  "Пол затрясся от такой унылой музыки",
  "centered Молодой человек, это не 18 век, вы бы хотя бы Контрданс-Экосез сыграли",
  "Доски перекрытия проваливаются и ты ломаешь шею о бетонные лаги",
  "end"
];


//TODO: scene
script["secret_podnos_wall"] = [
  "scene polka_opened fadeIn",
  "Рояль отодвигается и за ним отодвигается стенная панель",
  "На полке стоит поднос и ряд книжек",
  "Одна из книг с зеленой змеей привлекает твое внимание. Орнамент на подносе меняет форму чем ближе ты подходишь.",
  "jump secret_podnos_wall_choice"
];

script["secret_podnos_wall_choice"] = [
  {
    "Choice": {
      "0": {
        "Text": "Открыть книгу",
        "Do": "jump open_book",
        "Condition": function() {
          return storage.podnos.decorated === false;
        }
      },
      "1": {
        "Text": "Провести пальцем по подносу",
        "Do": "jump podnos_action"
      }
    }
  }
]


script["open_book"] = [
  "display image book",
  "p Содержимое похоже на стихи Маяковского",
  "Ты перелистываешь примерно до середины",
  "p Нормальное форматирование началось",
  "p <b>Декоратор это функция, принимающая на вход функцию и возвращающая функцию</b>",
  function() {
    storage.podnos.decorated = true;
    return true;
  },
  "p Эта книга не должна стоять здесь",
  "hide book",
  "Книжка, довольно увесистая, улетает в окно",
  "centered Снизу слышится чей-то крик",
  "p Закрою окно пока никто не заметил откуда оно вылетело",
  "scene podnos_only",
  "jump secret_podnos_wall_choice"
];

script["podnos_action"] = [
  {
    "Conditional": {
      "Condition": function() {
        return storage.podnos.decorated;
      },
      "True": "jump podnos_action_success",
      "False": "jump podnos_action_dead_end"
    }
  }
];

script["podnos_action_dead_end"] = [
  "Ты зацепил занозу в пластиковом подносе и порезался",
  "Поднос начал жадно впитывать твою кровь",
  "Не, ты думаешь он просто так красногшо цвета?",
  "Не в силах оторвать руку от подноса ты теряешь сознание",
  "Ты сдох. Пиздец лох",
  "end"
];

script["podnos_action_success"] = [
  "За окном резко темнеет",
  "jump redbull_sanctuary"
];
