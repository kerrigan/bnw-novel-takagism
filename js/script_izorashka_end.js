function good_komar() {
  return (!storage.izorashka_flat.arbuz_spoiled && storage.arbuz_cave.extension_cord) && !good_mn() && !good_slavik();
}

function good_mn() {
  return (storage.izorashka_flat.arbuz_spoiled && storage.arbuz_cave.yoghurt) && !good_komar() && !good_slavik();
}

function good_slavik() {
  return (storage.izorashka_kitchen.strange_package_small_taken && storage.arbuz_cave.drazhe) && !good_mn() && !good_komar();
}

script["komar_end"] = [
  {
    "Conditional": {
      "Condition": function() {
        return good_komar();
      },
      "True": "jump komar_good_end",
      "False": "jump komar_bad_end"
    }
  }
];

script["komar_good_end"] = [
  "scene black",
  "p Заебаться - CHECKED",
  "А больше в этой жизни ничего не надо",
  "jump credits"
];

script["komar_bad_end"] = [
  "scene red",
  "Газовый котел взорвался, в частном доме рухнула крыша, собаку раздавила машина",
  "jump credits"
];


script["mn_end"] = [
  {
    "Conditional": {
      "Condition": function() {
        return good_mn();
      },
      "True": "jump mn_good_end",
      "False": "jump mn_bad_end"
    }
  }
];

script["mn_good_end"] = [
  "scene black",
  "Щиткоин вырос, эродин ЕДЕТ, в инстаграме фотки лайкают. Жизнь замечательна",
  "p Пойду напишу еще один пост про превосходство щиткоинов над фиатом",
  "jump credits"
];

script["mn_bad_end"] = [
  "scene red",
  "centered Тебя выебали на работе за проебанный дедлайн",
  "centered Злобный админ заблочил твой любимый микроблог с анонимусами",
  "jump credits"
];


script["slavik_end"] = [
  {
    "Conditional": {
      "Condition": function() {
        return good_slavik();
      },
      "True": "jump slavik_good_end",
      "False": "jump slavik_bad_end"
    }
  }
];

script["slavik_good_end"] = [
  "scene black",
  "p Дешевая упорка и побочек мало",
  "p А Х У И Т Е Л Ь Н О",
  "jump credits"
];

script["slavik_bad_end"] = [
  "scene red",
  "Доллар пробил дно и тебя нашли сгнившим в Перово",
  "jump credits"
];

script["credits"] = [
  "scene title",
  "centered Визуальная новелла разработана в сотрудничестве BnW и Ironical Productions. 2018 год.",
  "end"
];
