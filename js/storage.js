"use strict";
// Persistent Storage Variable

let storage = {
	player: {
		name: "",
		fatigue: 0,
		thirst: 10,
		glucose: 50
	},
	izorashka_flat: {
		first: true,
		arbuz_spoiled: false,
		backpack_spoiled: false,
		multitool_taken: false,
		plenka_taken: false,
		plenka_used: false
	},
	izorashka_kitchen: {
		first: true,
		oat_taken: false,
		soy_taken: false,
		strange_package_small_taken: false,
		strange_package_big_taken: false,
		doshirak_taken: false
	},
	izorashka_eaves: {
		first: true,
		mini_potion_taken: false
	},
	arbuz_cave: {
		yoghurt: false,
		drazhe: false,
		extension_cord: false
	},
	podnos: {
		decorated: false
	}
};
