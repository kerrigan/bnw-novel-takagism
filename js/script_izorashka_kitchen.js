script["Izorashka_Kitchen"] = [
  "scene kitchen",
  "TODO: описание кухни и всякой еды там",
  "jump Izorashka_Kitchen_choice"
]

function muesli_ready() {
  var oat = storage.izorashka_kitchen.oat_taken;
  var soy = storage.izorashka_kitchen.soy_taken;
  var strange_big = storage.izorashka_kitchen.strange_package_big_taken;
  return oat && soy && strange_big;
};

function doshirak_ready() {
  return storage.izorashka_kitchen.doshirak_taken;
}

script["Izorashka_Kitchen_choice"] = [
  {
    "Choice": {
      "Dialog": "Ты стоишь на кухне",
      "1": {
        "Text": "Овсяные хлопья",
        "Do": "jump take_oat",
        "Condition": function() {
          return (storage.izorashka_kitchen.oat_taken === false) && !doshirak_ready();
        }
      },
      "2": {
        "Text": "Соевый фарш",
        "Do": "jump take_soy",
        "Condition": function() {
          return (storage.izorashka_kitchen.soy_taken === false) && !doshirak_ready();
        }
      },
      "3": {
        "Text": "Странные пакетики(полные)",
        "Do": "jump take_strange_package_big",
        "Condition": function() {
          return (storage.izorashka_kitchen.strange_package_big_taken === false) && !doshirak_ready();
        }
      },
      "4": {
        "Text": "Странные пакетики(почти пустые)",
        "Do": "jump take_strange_package_small",
        "Condition": function() {
          return (storage.izorashka_kitchen.strange_package_small_taken === false) && !doshirak_ready();
        }
      },
      "5": {
        "Text": "Доширак",
        "Do": "jump take_doshirak",
        "Condition": function() {
          return (storage.izorashka_kitchen.doshirak_taken === false) && !muesli_ready();
        }
      },
      "6": {
        "Text": "Поставить чайник",
        "Do": "jump start_boil_kettle",
        "Condition": function() {
          return muesli_ready() || doshirak_ready();
        }
      }
    }
  }
];


script["take_oat"] = [
  "display image oat",
  "Ты взял овсянку, первый сорт, самые крупные хлопья",
  "hide oat",
  function() {
    storage.izorashka_kitchen.oat_taken = true;
    return true;
  },
  "jump Izorashka_Kitchen_choice"
]

script["take_soy"] = [
  "display image soy",
  "Ты взял соевый фарш. Выглядит как отходы жизнедеятельности деревянной собаки.",
  "hide soy",
  function() {
    storage.izorashka_kitchen.soy_taken = true;
    return true;
  },
  "jump Izorashka_Kitchen_choice"
];

script["take_strange_package_big"] = [
  "display image ziplock_big",
  "Не знаю что это, но тут достаточно, чтобы достать щепотку",
  "hide ziplock_big",
  function() {
    storage.izorashka_kitchen.strange_package_big_taken = true;
    return true;
  },
  "jump Izorashka_Kitchen_choice"
];

script["take_strange_package_small"] = [
  "display image ziplock_small",
  "Не знаю что это, тут совсем на донышке",
  "hide ziplock_small",
  function() {
    storage.izorashka_kitchen.strange_package_small_taken = true;
    return true;
  },
  "jump Izorashka_Kitchen_choice"
];

script["take_doshirak"] = [
  "display image doshirak",
  "Быстрорастворимая лапша со вкусом Том-Ям. Какой-то виабу забыл.",
  "hide doshirak",
  function() {
    storage.izorashka_kitchen.doshirak_taken = true;
    return true;
  },
  "jump Izorashka_Kitchen_choice"
];

//TODO: звук
script["start_boil_kettle"] = [
  "Чайник закипает...",
  {
    "Conditional": {
      "Condition": function() {
        return muesli_ready();
      },
      "True": "jump eat_muesli",
      "False": "jump eat_doshirak"
    }
  }
];

script["eat_doshirak"] = [
  "p Очень вкусно, закажу себе как вернусь домой",
  "TODO: красная вспышка",
  "centered Тебя скручивает и ты падаешь на пол",
  "TODO: фон",
  "Ты сдох. Пиздец ты лох.",
  "end"
];

script["eat_muesli"] = [
  "p Похоже на овсяную кашу, которая по утрам",
  "Ты берешь ложку и зачерпываешь содержимое тарелки",
  "И отправляешь ее в рот",
  "p В том пакетике были НЕ ЭЛЕКТРОЛИТЫ",
  "jump mountain"
];
