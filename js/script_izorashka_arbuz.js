
//Вход
//TODO: сцена
script["Izorashka_Arbuz_pre"] = [
  "Ты уменьшаешься",
  "scene flat_arbuz_small",
  "p Теперь я стал ростом с весы",
  "Вокруг арбуза конструкция похожая на лестницу",
  {
    "Conditional": {
      "Condition": function() {
        return storage.izorashka_flat.arbuz_spoiled;
      },
      "True": "jump Izorashka_spoiled_arbuz",
      "False": "jump Izorashka_good_arbuz"
    }
  }
];

script["Izorashka_spoiled_arbuz"] = [
  {
    "Choice": {
      "Dialog": "Какие твои действия, мой низкий друг?",
      "0": {
        "Text": "Завернуться в пленку",
        "Do": "jump plenka_action",
        "Condition": function() {
          return storage.izorashka_flat.plenka_taken && !storage.izorashka_flat.plenka_used;
        }
      },
      "1": {
        "Text": "Залезть в арбуз",
        "Do": "jump goto_arbuz"
      }
    }
  }
];

script["goto_arbuz"] = [
  {
    "Conditional": {
      "Condition": function() {
        return storage.izorashka_flat.plenka_used;
      },
      "True": "jump with_plenka_in_arbuz",
      "False": "jump dead_end_arbuz_zhizha"
    }
  }
]

script["plenka_action"] = [
  function() {
    storage.izorashka_flat.plenka_used = true;
    return true;
  },
  "jump Izorashka_spoiled_arbuz"
];


script["with_plenka_in_arbuz"] = [
  "Ты завернулся в пленку аки школьник, сбежавший выживать в леса",
  "И полез в арбуз",
  "jump Izorashka_Arbuz"
];

script["Izorashka_good_arbuz"] = [
  "Ты лезешь в арбуз",
  "jump Izorashka_Arbuz"
];



//Основная сцена
//TODO: scene
script["Izorashka_Arbuz"] = [
  "scene arbuz_cave fadeIn",
  "Выход наружу закрывается",
  "p Лолшто, я что ебанутый, зачем я сюда залез?",
  "show k Normal",
  "k Ле, ты спалил место, где я пишу свои охуительные пасты",
  "k Короч, я выдаю тебе итем на выбор как в эрпоге и выпускаю наружу, а ты никому не рассказываешь",
  {
    "Choice": {
      "Dialog": "Что возьмешь?",
      "0": {
        "Text": "Баночка йогурта",
        "Do": "jump banochka_action"
      },
      "1": {
        "Text": "Крафтовый удлинитель",
        "Do": "jump ext_cord_action"
      },
      "2": {
        "Text": "Освежающее драже",
        "Do": "jump drazhe_action"
      }
    }
  }
];

script["banochka_action"] = [
  "display image yoghurt",
  "Ты положил стаканчик с йогуртом, стараясь не проткнуть его ключами, в свой бездонный карман",
  "p Это я точно есть не буду",
  function() {
    storage.arbuz_cave.yoghurt = true;
    return true;
  },
  "jump Izorashka_Arbuz_out"
];

script["ext_cord_action"] = [
  "display image extension_cord",
  "Ты взял крафтовый удлинитель",
  "p Не стоит это втыкать в розетку, там провод оголенный",
  function() {
    storage.arbuz_cave.extension_cord = true;
    return true;
  },
  "jump Izorashka_Arbuz_out"
];

script["drazhe_action"] = [
  "display image drazhe",
  "Ты положил дражайжую баночку с освежающим драже в свой бездонный карман",
  "p Не люблю сладкое",
  function() {
    storage.arbuz_cave.drazhe = true;
    return true;
  },
  "jump Izorashka_Arbuz_out"
];

script["Izorashka_Arbuz_out"] = [
  "hide k",
  "Пещеру заполняет дым и выход снова открыт",
  "Ты вылезаешь из этого странного места",
  "jump Izorashka_Flat3"
];

script["dead_end_arbuz_zhizha"] = [
  "clear",
  "scene #ff0000",
  "Ты помер утонув в белой жиже.",
  "end"
]
