"use strict";

/* exported messages */
/* exported notifications */
/* exported particles */
/* exported music */
/* exported voice */
/* exported sound */
/* exported videos */
/* exported images */
/* exported scenes */
/* exported characters */
/* exported script */

/* global storage */

// Define the messages used in the game.
let messages = {
	"Help": {
		"Title": "Help",
		"Subtitle": "Some useful Links",
		"Message": "<p><a href='https://monogatari.io/documentation/'>Documentation</a> - Everything you need to know.</p><p><a href='https://monogatari.io/demo/'>Demo</a> - A simple Demo.</p>"
	}
};

// Define the notifications used in the game
let notifications = {
	"Welcome": {
		title: "Welcome",
		body: "This is the Monogatari VN Engine",
		icon: ""
	}
};

// Define the Particles JS Configurations used in the game
let particles = {

};

// Define the music used in the game.
const music = {

};

// Define the voice files used in the game.
const voice = {

};

// Define the sounds used in the game.
const sound = {

};

// Define the videos used in the game.
const videos = {
	"intro_glitch": "intro_glitched.webm"
};

// Define the images used in the game.
const images = {
	"backpack": "backpack.png",
	"plenka": "plenka.png",
	"multitool": "multitool.png",
	"drazhe": "drazhe.png",
	"book": "book.png",
	"doshirak": "doshirak.png",
	"extension_cord": "extension_cord.png",
	"oat": "oat.png",
	"soy": "soy.png",
	"yoghurt": "yoghurt.png",
	"ziplock_big": "ziplock_big.png",
	"ziplock_small": "ziplock_small.png",
	"note_sheet": "note_sheet.png"
};

// Define the backgrounds for each scene.
const scenes = {
	"intro": "intro.png",
	"normal_flat": "normal_flat.png",
	"normal_flat_at_table": "normal_flat_at_table.png",
	"izorashka_flat": "izorashka_flat.png",
	"flat_wall": "flat_wall.png",
	"eaves": "eaves.png",
	"flat_arbuz_light": "flat_arbuz_light.png",
	"flat_arbuz_small": "flat_arbuz_small.png",
	"arbuz_cave": "arbuz_cave.png",
	"flat_speaker": "flat_speaker.png",
	"kitchen": "kitchen.png",
	"mountains": "mountains.png",
	"cliffs": "cliffs.png",
	"near_shelter": "near_shelter.png",
	"in_shelter": "in_shelter.png",
	"in_shelter_opened": "in_shelter_opened.png",
	"pre_podnos": "pre_podnos.png",
	"polka_opened": "polka_opened.png",
	"podnos_only": "podnos_only.png",
	"redbull_sanctuary": "redbull_sanctuary.png"
};

// Define the Characters
const characters = {
	"p": {
		"Name": "Ты",
		"Color": "#f00"
	},
	"a": {
		"Name": "???",
		"Color": "#ff0066",
		"Images": {
			"Normal": "4.png"
		}
	},
	"so": {
		"Name": "???",
		"Color": "#0af"
	},
	"l": {
		"Name": "???",
		"Color": "#66cc66",
		"Images": {
			"Normal": "l29ah_standing.png",
			"Pointing": "l29ah_pointing.png"
		}
	},
	"k": {
		"Name": "???",
		"Color": "#ff9900",
		"Images": {
			"Normal": "krkm.png"
		}
	}
};

let script = {
	// The game starts here.
	"Start": [
		"jump Intro"
	],
	"Intro": [
		"clear",
		"scene black with fadeIn",
		"centered Полгода назад ГГ познакомился с компанией, которая обитала на одном малоизвестном микроблоге.",
		"centered Сегодня они собираются ИРЛ. Это уже не в первый раз, так что ты спокойно закинул ляптоп в рюкзак и пошел на сходку.",
		"scene intro with fadeIn",
		"so Привет, ты пришел первым, у нас тут небольшой бардак, скоро уберем.",
		"p Давай помогу, что делать?",
		"centered Ты помог убраться в комнате",
		"centered Тем временем все собрались",
		"so Будешь? Мы угощаем",
		"p Не, спасибо, я как обычно",
		"На сходке всегда надо следить за своим стаканом и не пить что не наливаешь сам",
		"Особенно в незнакомом месте",
		"Ты отвлекся на типикал спор что лучше - Реакт или Ангуляр",
		"Но в этот раз тут был какой-то странный персонаж с длинными лохматыми волосами, который сидел подали от всех",
		"Воспользовавшись тем, что ты был отвлечен он взял ближайший пакетик с непонятным содержимым и высыпал в твой стакан",
		"show a Normal right with fadeIn",
		"a ...",
		"hide a fadeOut",
		"p О, я же не допил",
		"clear",
		"scene black with shake infinite",
		"play video intro_glitch",
		"jump Normal_Flat"
	],
	"Normal_Flat": [
		"clear",
		"scene normal_flat",
		"p Как же голова болит, что это было?",
		"jump Normal_Flat_Choice"
	],
	"Normal_Flat_Choice": [
		"Вы находитесь, в комнате с высоким потолком. Слева рояль. Справа телевизор и дверь. Перед вами стол.",
		{
			"Choice": {
				"Dialog": "Что делать?",
				"piano": {
					"Text": "Сыграть на рояле",
					"Do": "jump Nothing_piano",
				},
				"table": {
					"Text": "Подойди к столу",
					"Do": "jump Normal_Flat_At_Table",
				},
				"door": {
					"Text": "Открыть дверь",
					"Do": "jump Nothing_door"
				}
			}
		}
	],
	"Nothing_piano": [
		"Заебись сыграл. Нихуя не случилось",
		"jump Normal_Flat_Choice"
	],
	"Nothing_door": [
		"Дверь не открылась",
		"jump Normal_Flat_Choice"
	],

	"Normal_Flat_At_Table": [
		"scene normal_flat_at_table",
		"На столе весы и пакетик с неизвестным белым порошком. Еще стоит кружка с налитым чаем.",
		{
			"Choice": {
				"Dialog": "Твои действия?",
				"1": {
					"Text": "Высыпать на стол и снюхать.",
					"Do": "jump Bad_End"
				},
				"2": {
					"Text": "Высыпать в весы, отмерить 100 мг и снюхать.",
					"Do": "jump Izorashka_Flat"
				},
				"3": {
					"Text": "Высыпать в чай и выпить",
					"Do": "jump Bad_End"
				}
			}
		}
	],


	"Bad_End": [
		"clear",
		"scene #ff0000",
		"centered Ты сдох. Пиздец лох.",
		"end"
	]
};


/*
"Start": [
	"notify Welcome",
	{
		"Input": {
			"Text": "What is your name?",
			"Validation": function (input) {
				return input.trim().length > 0;
			},
			"Save": function (input) {
				storage.player.Name = input;
				return true;
			},
			"Warning": "You must enter a name!"
		}
	},

	"h Hi {{player.Name}} Welcome to Monogatari!",

	{
		"Choice": {
			"Dialog": "h Have you already read some documentation?",
			"Yes": {
				"Text": "Yes",
				"Do": "jump Yes"
			},
			"No": {
				"Text": "No",
				"Do": "jump No"
			}
		}
	}
],

"Yes": [

	"h That's awesome!",
	"h Then you are ready to go ahead and create an amazing Game!",
	"h I can't wait to see what story you'll tell!",
	"end"
],

"No": [

	"h You can do it now.",

	"display message Help",

	"h Go ahead and create an amazing Game!",
	"h I can't wait to see what story you'll tell!",
	"end"
]
*/
