//TODO: сцена
script["Izorashka_Flat2"] = [
  "scene flat_arbuz_light",
  "В комнате стало прохладнее и свежий воздух заполнил эти высокие полотки",
  "p Надо чаще проветривать",
  "Арбуз на столе засветился в области вырезанного куска",
  "p Я в какое-то дешевое фентези попал? Теперь буду следить за тем что пью.",
  "jump Izorashka_Flat2_choice"
];

script["Izorashka_Flat2_choice"] = [
  {
    "Choice": {
      "Dialog": "Твои действия?",
      "0": {
        "Text": "Отмерить 5мг и выпить",
        "Do": "jump Izorashka_Arbuz_pre",
        "Condition": function() {
          return storage.izorashka_eaves.mini_potion_taken;
        }
      },
      "1": {
        "Text": "Отмерить 50мг и выпить",
        "Do": "jump dead_end_arbuz_mg",
        "Condition": function() {
          return storage.izorashka_eaves.mini_potion_taken;
        }
      },
      "2": {
        "Text": "Отмерить 100мг и выпить",
        "Do": "jump dead_end_arbuz_mg",
        "Condition": function() {
          return storage.izorashka_eaves.mini_potion_taken;
        }
      },
      "3": {
        "Text": "Засунуть в арбуз руку",
        "Do": "jump arbuz_switch"
      }
    }
  }
];

script["arbuz_switch"] = [
  "Внутри арбуза какой-то переключатель",
  {
    "Choice": {
      "1": {
        "Text": "ЖМИ!",
        "Do": "jump dead_end_arbuz_switch"
      }
    }
  }
];

//Смерть от смены гравитации
script["dead_end_arbuz_switch"] = [
  "Ты сдох. Пиздец лох",
  "end"
];

//Смерть от передозировки
script["dead_end_arbuz_mg"] = [
  "Ты сдох. Пиздец лох",
  "end"
];
